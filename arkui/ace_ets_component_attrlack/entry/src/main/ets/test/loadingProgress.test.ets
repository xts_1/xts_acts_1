/**
 * Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';
import {describe, beforeAll, beforeEach, afterEach, afterAll, it, expect} from "hypium/index"
import Utils from './Utils.ets'

export default function loadingProgressCircularJsunit() {
  describe('loadingProgressCircularTest', function () {
    beforeEach(async function (done) {
      console.info("flex beforeEach start");
      let options = {
        uri: 'pages/loadingProgress',
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get loadingProgress state success " + JSON.stringify(pages));
        if (!("loadingProgress" == pages.name)) {
          console.info("get loadingProgress state success " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await Utils.sleep(2000);
          console.info("push loadingProgress page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push loadingProgress page error: " + err);
      }
      done()
    });

    afterEach(async function () {
      await Utils.sleep(1000);
      console.info("loadingProgressCircular after each called");
    });

    /*
     * @tc.number       SUB_ACE_BASIC_ETS_API_0011
     * @tc.name         testloadingProgressCircular0011
     * @tc.desic         aceloadingProgressCircularEtsTest0011
     */
    it('testloadingProgressCircular0011', 0, async function (done) {
      console.info('loadingProgressCircular testloadingProgressCircular0010 START');
      await Utils.sleep(2000);
      let strJson = getInspectorByKey('CircularText');
      console.info("[testloadingProgressCircular0011] component textAlign strJson:" + strJson);
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Text');
      expect(obj.$attrs.color).assertEqual(undefined);
      expect(obj.$attrs.width).assertEqual('100.00vp');
      expect(obj.$attrs.margin).assertEqual('0.00px');
      console.info("[testloadingProgressCircular0011] color value :" + obj.$attrs.color);
      console.info("[testloadingProgressCircular0011] width value :" + obj.$attrs.width);
      console.info("[testloadingProgressCircular0011] margin value :" + obj.$attrs.margin);
      done();
    });

    /*
     * @tc.number       SUB_ACE_BASIC_ETS_API_0001
     * @tc.name         testloadingProgressCircular0001
     * @tc.desic         aceloadingProgressCircularEtsTest0011
     */
    it('testloadingProgressOrbital0001', 0, async function (done) {
      console.info('loadingProgressOrbital testloadingProgressCircular0001 START');
      await Utils.sleep(2000);
      let strJson = getInspectorByKey('OrbitalText');
      console.info("[testloadingProgressOrbital0001] component textAlign strJson:" + strJson);
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Text');
      expect(obj.$attrs.color).assertEqual(undefined);
      expect(obj.$attrs.width).assertEqual('100.00vp');
      expect(obj.$attrs.margin).assertEqual('0.00px');
      console.info("[testloadingProgressOrbital0001] color value :" + obj.$attrs.color);
      console.info("[testloadingProgressOrbital0001] width value :" + obj.$attrs.width);
      console.info("[testloadingProgressOrbital0001] margin value :" + obj.$attrs.margin);
      done();
    });
  })
}
